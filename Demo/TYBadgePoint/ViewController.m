//
//  ViewController.m
//  TYBadgePoint
//
//  Created by 夏伟 on 2016/12/22.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import <TYUIKitAdditions/UIBarButtonItem+TYAdditions.h>
#import "UIBarButtonItem+TYBadge.h"
#import "UIBarButtonItem+TYBadgeStyle.h"
#import "UIView+TYBadgePoint.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *badgeView;
@property (weak, nonatomic) IBOutlet UIView *pointView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    UIBarButtonItem *unReadItem = [UIBarButtonItem ty_itemWithImage:[UIImage imageNamed:@"timeBtn_Nav"] target:nil action:nil];
    self.navigationItem.rightBarButtonItem = unReadItem;
    [self.navigationItem.rightBarButtonItem ty_badgeStyle];
    self.navigationItem.rightBarButtonItem.badgeValue = @"2";

    [self.badgeView ty_addBadgeTip:@"1"];
    //    [self.badgeView ty_addBadgeTip:@"4" withCenterPosition:CGPointMake(25, 25)];

    [self.pointView ty_addBadgePointWithPointPosition:CGPointMake(100, 15)];
    //    [self.pointView ty_addBadgePoint:5 withPosition:TYBadgePositionTypeMiddle];
}

@end
