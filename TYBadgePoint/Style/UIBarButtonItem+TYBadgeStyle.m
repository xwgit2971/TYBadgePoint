//
//  UIBarButtonItem+TYBadgeStyle.m
//  BadgeDemo
//
//  Created by 夏伟 on 2016/12/22.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "M13BadgeView+TYStyle.h"
#import "UIBarButtonItem+TYBadgeStyle.h"
#import <TYUIKitAdditions/UIDevice+TYAdditions.h>
#import "UIBarButtonItem+TYBadge.h"

@implementation UIBarButtonItem (TYBadgeStyle)

- (void)ty_badgeStyle {
    self.badgeTextColor = [UIColor whiteColor];
    self.badgeBGColor = kColorF7;
    if ([UIDevice ty_iphone6] || [UIDevice ty_iphone6Plus]) {
        self.badgeFont = [UIFont boldSystemFontOfSize:12];
    } else {
        self.badgeFont = [UIFont boldSystemFontOfSize:11];
    }
    self.shouldAnimateBadge = NO;
}

@end
