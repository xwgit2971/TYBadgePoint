//
//  UIBarButtonItem+TYBadgeStyle.h
//  BadgeDemo
//
//  Created by 夏伟 on 2016/12/22.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (TYBadgeStyle)

- (void)ty_badgeStyle;

@end
