//
//  M13BadgeView+TYStyle.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/19.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "M13BadgeView+TYStyle.h"
#import "UIDevice+TYAdditions.h"

@implementation M13BadgeView (TYStyle)

- (void)ty_badgeStyle {
    self.textColor = [UIColor whiteColor];
    self.badgeBackgroundColor = kColorF7;
    self.animateChanges = NO;
    
    if ([UIDevice ty_iphone6] || [UIDevice ty_iphone6Plus]) {
        self.font = [UIFont boldSystemFontOfSize:12];
    } else {
        self.font = [UIFont boldSystemFontOfSize:11];
    }
}

@end
