//
//  TYBadgeView+TYStyle.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/19.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <M13BadgeView/M13BadgeView.h>
#import <UIColor-TYAdditions/UIColor+TYAdditions.h>

#define kColorF7 [UIColor add_colorWithRGBHexString:@"0xf75388"] // 小红点和Badge 背景色

@interface M13BadgeView (TYStyle)

- (void)ty_badgeStyle;

@end
