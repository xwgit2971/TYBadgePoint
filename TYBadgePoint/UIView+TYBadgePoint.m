//
//  UIView+TYBadgePoint.m
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/19.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "UIView+TYBadgePoint.h"
#import "M13BadgeView+TYStyle.h"
#import <UIColor-TYAdditions/UIColor+TYAdditions.h>

#define kTagBadgeView 1000
#define kTagBadgePointView 1001

@implementation UIView (TYBadgePoint)

- (void)ty_addBadgeTip:(NSString *)badgeValue {
    if (!badgeValue || !badgeValue.length) {
        [self ty_removeBadgeTip];
    } else {
        UIView *badgeV = [self viewWithTag:kTagBadgeView];
        if (badgeV && [badgeV isKindOfClass:[M13BadgeView class]]) {
            [(M13BadgeView *)badgeV setText:badgeValue];
            badgeV.hidden = NO;
        } else {
            badgeV = [[M13BadgeView alloc] init];
            [(M13BadgeView *)badgeV ty_badgeStyle];
            [(M13BadgeView *)badgeV setText:badgeValue];
            badgeV.tag = kTagBadgeView;
            [self addSubview:badgeV];
        }
        CGSize badgeSize = badgeV.frame.size;
        CGSize selfSize = self.frame.size;
        CGFloat offset = 2.0;
        [badgeV setCenter:CGPointMake(selfSize.width- (offset+badgeSize.width/2),
                                      (offset +badgeSize.height/2))];
    }
}

- (void)ty_addBadgeTip:(NSString *)badgeValue withCenterPosition:(CGPoint)center {
    if (!badgeValue || !badgeValue.length) {
        [self ty_removeBadgeTip];
    } else {
        UIView *badgeV = [self viewWithTag:kTagBadgeView];
        if (badgeV && [badgeV isKindOfClass:[M13BadgeView class]]) {
            [(M13BadgeView *)badgeV setText:badgeValue];
            badgeV.hidden = NO;
        } else {
            badgeV = [[M13BadgeView alloc] init];
            [(M13BadgeView *)badgeV ty_badgeStyle];
            [(M13BadgeView *)badgeV setText:badgeValue];
            badgeV.tag = kTagBadgeView;
            [self addSubview:badgeV];
        }
        [badgeV setCenter:center];
    }
}

- (void)ty_removeBadgeTip {
    NSArray *subViews =[self subviews];
    if (subViews && [subViews count] > 0) {
        for (UIView *aView in subViews) {
            if (aView.tag == kTagBadgeView && [aView isKindOfClass:[M13BadgeView class]]) {
                aView.hidden = YES;
            }
        }
    }
}

- (void)ty_addBadgePointWithPosition:(TYBadgePositionType)type {
    [self ty_addBadgePoint:4 withPosition:type];
}

- (void)ty_addBadgePointWithPointPosition:(CGPoint)point {
    [self ty_addBadgePoint:4 withPointPosition:point];
}

- (void)ty_addBadgePoint:(NSInteger)pointRadius withPosition:(TYBadgePositionType)type {
    if(pointRadius < 1)
        return;
    [self ty_removeBadgePoint];

    UIView *badgeView = [[UIView alloc]init];
    badgeView.tag = kTagBadgePointView;
    badgeView.layer.cornerRadius = pointRadius;
    badgeView.backgroundColor = kColorF7;

    switch (type) {
        case TYBadgePositionTypeMiddle:
            badgeView.frame = CGRectMake(0, self.frame.size.height / 2 - pointRadius, 2 * pointRadius, 2 * pointRadius);
            break;
        default:
            badgeView.frame = CGRectMake(self.frame.size.width - 2 * pointRadius, 0, 2 * pointRadius, 2 * pointRadius);
            break;
    }

    [self addSubview:badgeView];
}

- (void)ty_addBadgePoint:(NSInteger)pointRadius withPointPosition:(CGPoint)point {
    if(pointRadius < 1)
        return;
    [self ty_removeBadgePoint];

    UIView *badgeView = [[UIView alloc]init];
    badgeView.tag = kTagBadgePointView;
    badgeView.layer.cornerRadius = pointRadius;
    badgeView.backgroundColor = kColorF7;
    badgeView.frame = CGRectMake(0, 0, 2 * pointRadius, 2 * pointRadius);
    badgeView.center = point;
    [self addSubview:badgeView];
}

- (void)ty_removeBadgePoint {
    for (UIView *subView in self.subviews) {
        if(subView.tag == kTagBadgePointView) {
            [subView removeFromSuperview];
        }
    }
}

@end
