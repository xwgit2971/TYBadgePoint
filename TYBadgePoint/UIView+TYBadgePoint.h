//
//  UIView+TYBadgePoint.h
//  TYUIKitAdditions
//
//  Created by 夏伟 on 2016/10/19.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TYBadgePositionType) {
    TYBadgePositionTypeDefault = 0,
    TYBadgePositionTypeMiddle
};

@interface UIView (TYBadgePoint)

#pragma mark - 添加Badge (@"1" / @"未读"等文字)
- (void)ty_addBadgeTip:(NSString *)badgeValue; // CenterPosition: 右上角
- (void)ty_addBadgeTip:(NSString *)badgeValue withCenterPosition:(CGPoint)center;
- (void)ty_removeBadgeTip;

#pragma mark - 添加小红点
- (void)ty_addBadgePointWithPosition:(TYBadgePositionType)type; // pointRadius: 4
- (void)ty_addBadgePointWithPointPosition:(CGPoint)point;
- (void)ty_addBadgePoint:(NSInteger)pointRadius withPosition:(TYBadgePositionType)type;
- (void)ty_addBadgePoint:(NSInteger)pointRadius withPointPosition:(CGPoint)point;
- (void)ty_removeBadgePoint;

@end
