Pod::Spec.new do |s|
  s.name = 'TYBadgePoint'
  s.version = '0.0.2'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = '添加小红点和Badge'
  s.homepage = 'https://gitlab.com/xwgit2971/TYBadgePoint'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYBadgePoint.git', :tag => s.version }
  s.source_files = 'TYBadgePoint/*.{h,m}', 'TYBadgePoint/Style/*.{h,m}'
  s.framework = 'UIKit'
  s.requires_arc = true
  s.dependency  'TYUIKitAdditions'
  s.dependency  "M13BadgeView", '~> 1.0.4'
end
